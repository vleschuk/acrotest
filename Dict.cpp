#include "Dict.h"
#include "AcroTestException.h"
#include "Helpers.h"
#include <cmath> // for abs()
#include <iostream>
#include <fstream>

Dict::Dict(const std::string &lang) :
	configFile_(lang + ".dict"),
	minusWord_(""),
	decNote_(""),
	octNote_("")
{
	ParseConfig();
}

void Dict::ParseConfig()
{
	std::ifstream inputFile(configFile_.c_str());
	if(!inputFile)
	{
		throw AcroTestException("Can't open dict file " + configFile_ + " for reading");
	}
	
	std::string line;
	while(getline(inputFile, line))
	{
		std::string key, value;
		SplitString(line, '=', key, value);
		if("minus" == key)
		{
			minusWord_ = value;
		}
		else if("dec_note" == key)
		{
			decNote_ = value;
		}
		else if("oct_note" == key)
		{
			octNote_ = value;
		}
		else
		{
			unsigned n = std::abs(StrToNum(key));
			numberMap_[n] = value;
		}
	}
}

const std::string & Dict::GetMinusWord() const
{
	return minusWord_;
}

std::string Dict::GetNumberWord(unsigned num) const
{
	NumberMapType::const_iterator it = numberMap_.find(num);
	if(it == numberMap_.end())
	{
		return "";
	}
	return it->second;
}

unsigned Dict::GetLowerBound(unsigned num) const
{
	unsigned lowerBound;
	for(NumberMapType::const_iterator it = numberMap_.begin(); it != numberMap_.end(); ++it)
	{
		if(it->first < num)
			lowerBound = it->first;
	}
	return lowerBound;
}

const std::string & Dict::GetDecNote() const
{
	return decNote_;
}

const std::string & Dict::GetOctNote() const
{
	return octNote_;
}
