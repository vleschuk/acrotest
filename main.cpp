#include <iostream>
#include <string>
#include "NumConverter.h"
#include "Helpers.h"
#include "AcroTestException.h"

int main(int argc, char **argv)
{
	try{
		NumConverter converter(argc < 2 ? "Russian" : argv[1]);
		while(!(std::cin.eof()))
		{
			try{
				std::string str;
				int num;
				std::cin >> str;
				num = StrToNum(str);
				const Dict *dict = converter.GetDict();
				std::cout << converter.Convert(num) << " " 
					<< dict->GetDecNote() << std::endl;
				std::cout << converter.Convert(DecToOct(num)) << " " 
					<< dict->GetOctNote() << std::endl;
			}
			catch(AcroTestException &e)
			{
				std::cout << "Error: " << e.what() << std::endl;
			}
		}
		
	}
	catch(AcroTestException &e)
	{
		std::cout << "Error: " << e.what() << std::endl;
		return 1;
	}
}
