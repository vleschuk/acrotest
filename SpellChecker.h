#ifndef SPELLCHECKER_H_
#define SPELLCHECKER_H_

#include <string>
#include <vector>
#include "AcroTestException.h"

class AcroTestSpellCheckerException: public AcroTestException
{
public:
	AcroTestSpellCheckerException(const std::string &s):
		::AcroTestException(s)
		{}
};


// Structure representing pairs of strings to be replaced
struct ReplacementStrings
{
	std::string toReplaceFirst;
	std::string toReplaceSecond;
	std::string replaceWithFirst;
	std::string replaceWithSecond;
};

// Class collecting string pairs to be replaced and checking input 
// strings using provided string grammars.
// Searches for corresponging *.rules text file in curr dir, if not found
// throws exception
class SpellChecker
{
public:
	SpellChecker(const std::string &);
	void CheckGrammar(std::vector<std::string> &);
private:
	void ParseConfig();
	std::string configFile_;
	std::vector<ReplacementStrings> rules_;
};
#endif // SPELLCHECKER_H_

