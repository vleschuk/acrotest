#ifndef ACROTEST_EXCEPTION_H_
#define ACROTEST_EXCEPTION_H_

#include <stdexcept>
#include <string>

class AcroTestException: public std::exception
{
public:
	AcroTestException(const std::string &);
	virtual ~AcroTestException() throw() {}
	const char *what();
private:
	char msg_[80];
};

#endif // ACROTEST_EXCEPTION_H_
