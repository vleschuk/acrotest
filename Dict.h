#ifndef DICT_H_
#define DICT_H_
#include <map>
#include <string>

// Dictionary class
// For given language string searches for corresponging *.dict text file
// and parses it for numeric words and special phrases. 
// If file not found exception is thrown
class Dict
{
public:
	Dict(const std::string &);
	
	// Retrieves "minus" word for current lang
	const std::string & GetMinusWord() const; 
	
	// Searches for a word corresponing to given number
	// returns word or empty string if not found
	std::string GetNumberWord(unsigned) const;
	
	// Get maximum number from dict close to the given one
	unsigned GetLowerBound(unsigned) const;
	
	// Returns "in decimal format" phrase for current lang
	const std::string & GetDecNote() const;
	
	// Returns "in octal format" phrase for current lang 
	const std::string & GetOctNote() const;
private:
	void ParseConfig();
	std::string configFile_;
	typedef std::map<unsigned, std::string> NumberMapType;
	NumberMapType numberMap_;
	std::string minusWord_;
	std::string decNote_;
	std::string octNote_;
};

#endif // DICT_H_
