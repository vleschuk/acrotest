#include <sstream>
#include <iostream>
#include <cmath>
#include "Helpers.h"
#include "AcroTestException.h"

int StrToNum(const std::string &str)
{
	std::istringstream ss(str);
	int num;
	if(!(ss >> num))
	{
		throw AcroTestException("Cannot convert string " + str + " to number");
	}
	return num;
}

void SplitString(const std::string &str, char delim, 
					std::string &key, std::string &value)
{
	size_t idx = str.find_first_of(delim);
	if(std::string::npos == idx)
	{
		throw AcroTestException("Cannot split string: " + str);
	}
	key = str.substr(0, idx);
	value = str.substr(idx+1);
}

int DecToOct(int num)
{
	std::vector<int> vec;
	for(int n = num; n != 0;)
	{
		vec.push_back(n%8);
		n = n/8;
	}
	int octalNumber = 0;
	int oldNumber = 0;
	for(size_t i = 0; i < vec.size(); ++i)
	{
		oldNumber = octalNumber;
		octalNumber += (int) pow(10, i) * vec[i];
		if(std::abs(octalNumber) < std::abs(oldNumber))
		{
			throw AcroTestException("Value too large for in octal format");
		}
	}
	return octalNumber;
}
