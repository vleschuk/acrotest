#include <fstream>
#include <iostream>
#include "SpellChecker.h"
#include "AcroTestException.h"
#include "Helpers.h"

SpellChecker::SpellChecker(const std::string &lang):
		configFile_(lang + ".rules")
{
	ParseConfig();
}

void SpellChecker::ParseConfig()
{
	std::ifstream inputFile(configFile_.c_str());
	if(!inputFile)
	{
		throw AcroTestSpellCheckerException("Can't open grammar file " + configFile_ + " for reading");
	}
	std::string line;

	while(std::getline(inputFile, line))
	{
		ReplacementStrings rule;
		std::string str1, str2;
		SplitString(line, '=', str1, str2);
		if(str1[0] == '^')
		{
			rule.toReplaceFirst = "";
			rule.toReplaceSecond = str1.substr(1);
			rule.replaceWithFirst = str2;
			rule.replaceWithSecond = "";
			rules_.push_back(rule);
			continue;
		}
		
		SplitString(str1, ' ', rule.toReplaceFirst, rule.toReplaceSecond);
		SplitString(str2, ' ', rule.replaceWithFirst, rule.replaceWithSecond);
		rules_.push_back(rule);
	}
}

void SpellChecker::CheckGrammar(std::vector<std::string> &sentence)
{
	for(std::vector<ReplacementStrings>::const_iterator it = rules_.begin();
		it != rules_.end(); ++it)
	{
		
		if(it->toReplaceFirst.empty() && 
			sentence[0] == it->toReplaceSecond)
		{
			sentence[0] = it->replaceWithFirst;
			continue;
		}
		for(std::vector<std::string>::size_type i = 1;
			i < sentence.size(); ++i)
		{
			if(sentence[i-1] == it->toReplaceFirst &&
				sentence[i] == it->toReplaceSecond)
			{
				sentence[i-1] = it->replaceWithFirst;
				sentence[i] = it->replaceWithSecond;
			}
		}
	}
}
