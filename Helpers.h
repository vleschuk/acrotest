#ifndef HELPERS_H_
#define HELPERS_H_
#include <string>
#include <vector>

int StrToNum(const std::string &);
void SplitString(const std::string &str, char delim, 
					std::string &key, std::string &value); 
int DecToOct(int);

#endif // HELPERS_H_
