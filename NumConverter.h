#ifndef NUM_CONVERTER_H_
#define NUM_CONVERTER_H_

#include <string>
#include "Dict.h"
#include "SpellChecker.h"

// NumConverter class
// For a chosen language (constructor arg)
// Converts given integer to word phrases
class NumConverter
{
public:
	NumConverter(const std::string &);
	~NumConverter();
	std::string Convert(int);
	const Dict *GetDict(); 
private:
	void GetElems(int, std::vector<std::string> &);
	Dict dict_;
	SpellChecker * pChecker_;
};

#endif // NUM_CONVERTER_H_
