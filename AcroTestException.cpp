#include <cstring>
#include <string>
#include "AcroTestException.h"

AcroTestException::AcroTestException(const std::string &msg)
{
	strncpy(msg_, msg.c_str(), sizeof(msg_));
	msg_[sizeof(msg_)-1] = 0;
}

const char * AcroTestException::what()
{
	return msg_;
}
