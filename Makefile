CFLAGS += -Wall -Wextra -I. -g -fPIE
AcroTest: main.o NumConverter.o Dict.o Helpers.o AcroTestException.o SpellChecker.o
	$(CXX) $(CFLAGS) \
	main.o \
	NumConverter.o \
	Dict.o \
	Helpers.o \
	AcroTestException.o \
	SpellChecker.o \
	-o $@

main.o:
	$(CXX) $(CFLAGS) -c main.cpp

NumConverter.o:
	$(CXX) $(CFLAGS) -c NumConverter.cpp

Dict.o:
	$(CXX) $(CFLAGS) -c Dict.cpp
	
Helpers.o:
	$(CXX) $(CFLAGS) -c Helpers.cpp

AcroTestException.o:
	$(CXX) $(CFLAGS) -c AcroTestException.cpp

SpellChecker.o:
	$(CXX) $(CFLAGS) -c SpellChecker.cpp

clean:
	rm -f AcroTest *.o
