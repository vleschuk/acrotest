#include <cmath>
#include <iostream>
#include "NumConverter.h"
#include "Helpers.h"

NumConverter::NumConverter(const std::string &lang) :
	dict_(lang),
	pChecker_(0)
{
	try
	{
		pChecker_ = new SpellChecker(lang);
	}
	catch(AcroTestSpellCheckerException &e)
	{
		std::cout << "Warning! Spell checking not available: " << e.what() << std::endl;
	}
}

NumConverter::~NumConverter()
{
	if(pChecker_)
		delete pChecker_;
}

std::string NumConverter::Convert(int num)
{
	std::vector<std::string> strElems;
	GetElems(num, strElems);
	std::string s("");
	for(std::vector<std::string>::const_iterator it = strElems.begin();
		it != strElems.end(); ++it)
		s += *it + " ";
	s = s.substr(0, s.size() - 1);
	
	return s;
}

void NumConverter::GetElems(int num, std::vector<std::string> &strElems)
{
	unsigned numberToConvert = std::abs(num); // only positive values are in dict	
	
	// Check whether we have negative value
	if(num < 0)
	{
		strElems.push_back(dict_.GetMinusWord());
	}

	std::string numWord = dict_.GetNumberWord(numberToConvert);
	if(numWord.empty())
	{
		// Trying to build it
		unsigned lowerBound = dict_.GetLowerBound(numberToConvert);

		if(numberToConvert / lowerBound > 1)
		{
			GetElems(numberToConvert / lowerBound, strElems);

		}
		strElems.push_back(dict_.GetNumberWord(lowerBound));
		unsigned restNum = numberToConvert % lowerBound;
		if(restNum != 0)
		{
			GetElems(restNum, strElems);
		}
	}
	else
	{
		strElems.push_back(numWord);
	}
	if(pChecker_)
		pChecker_->CheckGrammar(strElems);
}

const Dict *NumConverter::GetDict()
{
	return &dict_;
}
